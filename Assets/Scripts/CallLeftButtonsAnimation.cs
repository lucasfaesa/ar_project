﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallLeftButtonsAnimation : MonoBehaviour
{
    [Header("Manually Assigned")]

    [SerializeField]
    private Animator leftButtonsAnimator;

    [SerializeField]
    private GameObject buttonArrowIcon;

    [Header("Script Assigned")]
    [SerializeField]
    private bool isInsideCanvas;

    //usado pelo botao LeftButtonCaller
    public void PlayAnimationOnClick()
    {
        if (!isInsideCanvas)
        {
            isInsideCanvas = true;
            leftButtonsAnimator.Play("leftButtonsIN");
            buttonArrowIcon.transform.localScale = new Vector2(buttonArrowIcon.transform.localScale.x * -1, buttonArrowIcon.transform.localScale.y);
        }
        else
        {
            isInsideCanvas = false;
            leftButtonsAnimator.Play("leftButtonsOUT");
            buttonArrowIcon.transform.localScale = new Vector2(buttonArrowIcon.transform.localScale.x * -1, buttonArrowIcon.transform.localScale.y);
        }
    }

    private void OnEnable()
    {
        leftButtonsAnimator.Play("Normal State");
        isInsideCanvas = false;
    }
}
