﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementAndScaleButtonBehaviour : MonoBehaviour
{
    [Header("Script Assigned")]

    [SerializeField]
    private TouchController touchController;

    [SerializeField]
    private bool isBlocked;

    [Header("Manually Assigned")]

    [SerializeField]
    private Sprite originalBackground;

    [SerializeField]
    private Sprite blockedBackground;

    [SerializeField]
    private btTypes buttonTypes;

    private enum btTypes { horizontal, vertical, scale, movement};

    private void Start()
    {
        touchController = FindObjectOfType<TouchController>();
    }

    private void OnEnable()
    {
        ResetButtonStatus();
    }

    public void blockMovementOrRotation()
    {
        //on e off dos botoes
        if (!isBlocked)
        {
            isBlocked = true;
            this.gameObject.GetComponent<Button>().image.sprite = blockedBackground;

            switch (buttonTypes)
            {
                case btTypes.horizontal:
                    touchController.blockHorizontalRotation = true;
                    break;
                case btTypes.vertical:
                    touchController.blockVerticalRotation = true;
                    break;
                case btTypes.scale:
                    touchController.blockScale = true;
                    break;
                case btTypes.movement:
                    touchController.blockMovement = true;
                    break;
            }
        }
        else
        {
            isBlocked = false;
            this.gameObject.GetComponent<Button>().image.sprite = originalBackground;

            switch (buttonTypes)
            {
                case btTypes.horizontal:
                    touchController.blockHorizontalRotation = false;
                    break;
                case btTypes.vertical:
                    touchController.blockVerticalRotation = false;
                    break;
                case btTypes.scale:
                    touchController.blockScale = false;
                    break;
                case btTypes.movement:
                    touchController.blockMovement = false;
                    break;
            }
        }
    }

    private void ResetButtonStatus()
    {
        //retorna os botoes de movimento e escala ao estado off
        isBlocked = false;
        this.gameObject.GetComponent<Button>().image.sprite = originalBackground;

        if (touchController != null)
        {
            switch (buttonTypes)
            {
                case btTypes.horizontal:
                    touchController.blockHorizontalRotation = false;
                break;

                case btTypes.vertical:
                    touchController.blockVerticalRotation = false;
                break;

                case btTypes.scale:
                    touchController.blockScale = false;
                break;

                case btTypes.movement:
                    touchController.blockMovement = false;
                break;
            }
        }
    }
}
