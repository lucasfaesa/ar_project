﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField]
    private Transform ArCamera;
    // Start is called before the first frame update
    void Start()
    {
        ArCamera = GameObject.Find("ARCamera").GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(2* transform.position - ArCamera.position);
    }
}
