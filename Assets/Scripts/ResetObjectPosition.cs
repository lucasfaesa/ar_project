﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetObjectPosition : MonoBehaviour
{
    [Header("Manually Assigned")]

    [SerializeField]
    private Transform objectsHolder;

    [SerializeField]
    private Slider animationSlider;

    public void ResetPosition()
    {
        //coloca o objeto em uma posição definida pelo programador, caso objeto mude de tamanho no futuro, definir aqui
        objectsHolder.localPosition = new Vector3(0f, 0f, 1.5f);
        objectsHolder.localRotation = new Quaternion(0f, 0.15f, 0f, 1f);
        objectsHolder.localScale = new Vector3(1f, 1f, 1f);

        animationSlider.value = 0;
    }
}
