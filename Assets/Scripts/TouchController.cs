﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    [Header("Script Assigned")]

    //-- usado em MovementAndScaleButtonBehaviour --//                   
    public bool blockHorizontalRotation;
    public bool blockVerticalRotation;
    public bool blockScale;
    public bool blockMovement;

    //-- usado em SliderAnimationController --//
    public bool isSlinding;

    //-- usado em InfoButtonBehaviour --//    
    public bool blockRaycastPartInfo;

    private Touch touch;
    private Quaternion rotationY;
    private Quaternion rotationX;
    private Vector3 initialScale;

    private float moveSpeedModifier = 0.05f;
    private float rotateSpeedModifier = 0.1f;
    private float initialFingerDistance;

    bool clicked;

    private Vector3 Button0DownPosition, Button0UpPosition;

    [Header("Manually Assigned")]

    [SerializeField]
    private Transform trackedObject;

    [SerializeField]
    private LockButtonBehaviour lockButtonBehaviour;

    [SerializeField]
    private InfoButtonBehaviour infoButtonBehaviour;


    void Update()
    {
        if (lockButtonBehaviour.isLocked)
        {
            MoveAndRotate();
        }

        if (!infoButtonBehaviour.isDisabled)
        {
            ShowPartInfo();
        }

    }

    public void MoveAndRotate()
    {
        if (!blockHorizontalRotation && !isSlinding || !blockVerticalRotation && !isSlinding)
        {
            //movimentação horizontal e vertical no proprio eixo
            if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Moved)
                {
                    if (!blockHorizontalRotation)
                    {
                        rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * rotateSpeedModifier, 0f);
                        trackedObject.rotation = rotationY * trackedObject.transform.rotation;
                    }
                    if (!blockVerticalRotation)
                    {
                        rotationX = Quaternion.Euler(touch.deltaPosition.y * rotateSpeedModifier, 0f, 0f);
                        trackedObject.rotation = rotationX * trackedObject.transform.rotation;
                    }
                }
            }
        }
        if (!blockMovement && !isSlinding || !blockScale && !isSlinding)
        {
            //movimentacao espacial
            if (Input.touchCount == 2)
            {
                if (!blockMovement)
                {
                    touch = Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Moved)
                    {
                        Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

                        trackedObject.transform.Translate(touchDeltaPosition.x * moveSpeedModifier * Time.deltaTime,
                                                           touchDeltaPosition.y * moveSpeedModifier * Time.deltaTime, 0, Space.World);
                    }
                }

                if (!blockScale)
                {
                    //aumentar escala do objeto 

                    Touch touch1 = Input.GetTouch(0);
                    Touch touch2 = Input.GetTouch(1);

                    if (touch1.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Canceled
                        || touch2.phase == TouchPhase.Ended || touch2.phase == TouchPhase.Canceled)
                    {
                        return;
                    }

                    if (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began)
                    {
                        initialFingerDistance = Vector2.Distance(touch1.position, touch2.position);
                        initialScale = trackedObject.transform.localScale;
                    }
                    else
                    {
                        float currentFingersDistance = Vector2.Distance(touch1.position, touch2.position);

                        if (Mathf.Approximately(currentFingersDistance, 0)) return;

                        float scaleFactor = currentFingersDistance / initialFingerDistance;

                        trackedObject.transform.localScale = initialScale * scaleFactor;
                    }
                }
            }
        }
    }

    private void ShowPartInfo()
    {
        if (!blockRaycastPartInfo)
        {
            //mostrar informacoes das partes individuais do objeto

            if (Input.GetMouseButtonDown(0))
            {
                Button0DownPosition = Input.mousePosition;
                Debug.Log("Down: " + Button0DownPosition);
                clicked = true;
            }

            if (Input.GetMouseButtonUp(0) && clicked)
            {
                clicked = false;
                Button0UpPosition = Input.mousePosition;
                Debug.Log("Up: " + Button0DownPosition);

                if (Vector3.Distance(Button0DownPosition, Button0UpPosition) < 15f)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hitInfo;

                    if (Physics.Raycast(ray, out hitInfo))
                    {
                        Debug.Log(hitInfo.collider.gameObject.name);
                        hitInfo.collider.gameObject.GetComponent<ShowPartsInfo>().ShowPartsNameAndInfo();
                    }
                }
            }
        }        
    }
}
