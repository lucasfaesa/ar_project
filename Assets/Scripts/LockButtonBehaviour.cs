﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class LockButtonBehaviour : MonoBehaviour
{
    [Header("Script Assigned")]

    [SerializeField]
    private VuforiaBehaviour vuforiaBehaviour;

    [SerializeField]
    private DisableCamera disableCamera;

    [SerializeField]
    private ShowPartsInfo[] showPartsInfos;

    [SerializeField]
    private Renderer[] partsRenderers;

    [SerializeField]
    private List<float> outlinePartsValues;

    private Vector3 initialPosition, initialScale;
    private Quaternion initialRotation;

    //usado em TouchController
    public bool isLocked;

    [Header("Manually Assigned")]

    [SerializeField]
    private GameObject objectsHolder;

    [SerializeField]
    private GameObject imageTarget;

    [SerializeField]
    private GameObject infoButton;

    [SerializeField]
    private GameObject otherButtons;

    [SerializeField]
    private Sprite openSprite;

    [SerializeField]
    private Sprite lockedSprite;

    [SerializeField]
    private Sprite originalBackground;

    [SerializeField]
    private Sprite lockedBackground;

    [SerializeField]
    private UnityEngine.UI.Image padlockIcon;

    private void Start()
    {
        initialScale = objectsHolder.transform.localScale;
        initialRotation = objectsHolder.transform.localRotation;
        initialPosition = objectsHolder.transform.localPosition;

        this.gameObject.SetActive(false);
        otherButtons.SetActive(false);

        vuforiaBehaviour = FindObjectOfType<VuforiaBehaviour>();
        showPartsInfos = FindObjectsOfType<ShowPartsInfo>();
        disableCamera = FindObjectOfType<DisableCamera>();

        partsRenderers = objectsHolder.GetComponentsInChildren<Renderer>();

        //desativar outline das partes individuais
        for (int i = 0; i < partsRenderers.Length; i++)
        {
            if(partsRenderers[i].material.HasProperty("_SecondOutlineWidth"))
            {
                outlinePartsValues.Add(partsRenderers[i].material.GetFloat("_SecondOutlineWidth"));
                partsRenderers[i].material.SetFloat("_SecondOutlineWidth", 0);
            }
        }
    }

    public List<float> GetOutlineValues()
    {
        return outlinePartsValues;
    }

    //usado pelo botao Lock Button
    public void ChangeParentOnClick()
    {
        if (!isLocked)
        {
            isLocked = true;
            padlockIcon.sprite = openSprite;
            this.GetComponent<Button>().image.sprite = lockedBackground;

            //remove parentesco com o objeto "ImageTarget" para que o objeto escaneado possa permanecer na tela, mesmo fora da imagem escaneada
            objectsHolder.transform.parent = null;
            otherButtons.SetActive(true);
        }
        else
        {
            //caso camera esteja desativada quando o usuario clicar no cadeado, camera irá se ativar.
            if(!vuforiaBehaviour.enabled)
            {
                vuforiaBehaviour.enabled = true;
            }

            disableCamera.ResetCamera();

            isLocked = false;
            padlockIcon.sprite = lockedSprite;
            this.GetComponent<Button>().image.sprite = originalBackground;
            this.gameObject.SetActive(false);
            infoButton.SetActive(false);
            otherButtons.SetActive(false);

            //Retorna o objeto ao 'parent' original e devolve suas posições iniciais
            ReturnToOriginalSettings();

            //caso alguma informação de peça individual ainda estiver exibida no carro, elas são desativadas e outline zerado
            HidePartsInfoAndResetOutline();
        }
    }

    private void ReturnToOriginalSettings()
    {
        objectsHolder.transform.parent = imageTarget.transform;
        objectsHolder.transform.localPosition = initialPosition;
        objectsHolder.transform.localRotation = initialRotation;
        objectsHolder.transform.localScale = initialScale;

        //behaviour copiado do script do Vuforia, ele desativa os renderers, colliders e canvas que o objeto possuir, sem desativar o mesmo.
        HideTrackedObjects();
    }

    public void HidePartsInfoAndResetOutline()
    {
        foreach (ShowPartsInfo infos in showPartsInfos)
        {
            infos.HidePartsInfo();
        }

        //desativar outline das partes individuais
        for (int i = 0; i < partsRenderers.Length; i++)
        {
            partsRenderers[i].sharedMaterial.SetFloat("_SecondOutlineWidth", 0);
        }
    }

    private void HideTrackedObjects()
    {
        var rendererComponents = objectsHolder.GetComponentsInChildren<Renderer>(true);
        var colliderComponents = objectsHolder.GetComponentsInChildren<Collider>(true);
        var canvasComponents = objectsHolder.GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;
    }
}
