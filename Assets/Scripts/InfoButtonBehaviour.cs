﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InfoButtonBehaviour : MonoBehaviour
{
    [Header("Script Assigned")]

    [SerializeField]
    private ShowPartsInfo[] showPartsInfo;

    [SerializeField]
    private TouchController touchController;

    [SerializeField]
    private LockButtonBehaviour lockButtonBehaviour;

    [SerializeField]
    private Renderer[] partsRenderers;

    [SerializeField]
    private List<float> outlinePartsValues;

    //usado em TouchController
    public bool isDisabled;

    [Header("Manually Assigned")]

    [SerializeField]
    private GameObject objectsHolder;

    [SerializeField]
    private Sprite enabledSprite;

    [SerializeField]
    private Sprite disabledSprite;

    [SerializeField]
    private Image infoIcon;

    private void Awake()
    {
        this.gameObject.SetActive(false);
    }
    private void Start()
    {
        showPartsInfo = FindObjectsOfType<ShowPartsInfo>();
        touchController = FindObjectOfType<TouchController>();
        lockButtonBehaviour = FindObjectOfType<LockButtonBehaviour>();

        //bloqueio necessario para nao utilizar o raycast sem necessidade
       // touchController.blockRaycastPartInfo = true;

        partsRenderers = objectsHolder.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < showPartsInfo.Length; i++)
        {
            showPartsInfo[i].HidePartsInfo();
        }

        outlinePartsValues.AddRange(lockButtonBehaviour.GetOutlineValues());
    }

    private void OnEnable()
    {
        isDisabled = false;
        infoIcon.sprite = disabledSprite;

        for (int i = 0; i < showPartsInfo.Length; i++)
        {
            showPartsInfo[i].isInfoButtonDisabled = false;
            showPartsInfo[i].HidePartsInfo();
        }
        touchController.blockRaycastPartInfo = false;
    }

    public void InfoButtonOnClick()
    {
        if (!isDisabled)
        {
            //opcao informacao desativada, user ao clicar nas peças individuais NAO aparecem informacoes
            isDisabled = true;
            infoIcon.sprite = enabledSprite;

            touchController.blockRaycastPartInfo = true;

            //user ao clicar no botao, desativa todos os canvas e bloqueia o raycast
            for(int i = 0; i < showPartsInfo.Length; i++)
            {
                showPartsInfo[i].HidePartsInfo();
                showPartsInfo[i].isInfoButtonDisabled = true;
            }

            //desativa o outline dos objetos
            for (int i = 0; i < partsRenderers.Length; i++)
            {
                if (partsRenderers[i].material.HasProperty("_SecondOutlineWidth"))
                {
                    partsRenderers[i].sharedMaterial.SetFloat("_SecondOutlineWidth", 0);
                }
            }
        }
        else
        {
            //opcao informacao ativada, user ao clicar nas peças individuais exibe informacoes
            isDisabled = false;
            infoIcon.sprite = disabledSprite;

            touchController.blockRaycastPartInfo = false;

            for (int i = 0; i < showPartsInfo.Length; i++)
            {
                showPartsInfo[i].isInfoButtonDisabled = false;
            }

            //ativa o outline dos objetos
            for (int i = 0; i < partsRenderers.Length; i++)
            {
                if (partsRenderers[i].material.HasProperty("_SecondOutlineWidth"))
                {
                    partsRenderers[i].sharedMaterial.SetFloat("_SecondOutlineWidth", outlinePartsValues[i]);
                }
            }
        }
    }
}
