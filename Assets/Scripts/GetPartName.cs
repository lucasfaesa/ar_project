﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPartName : MonoBehaviour
{
    //script usado dentro do text, no prefab "PartInfoCanvas"

    void Start()
    {
        //pega o nome da parte escrita na peça individual do objeto escaneado
        string partName = transform.parent.parent.parent.GetComponent<ShowPartsInfo>().partName.ToString();
        this.GetComponent<Text>().text = partName;
    }
}
