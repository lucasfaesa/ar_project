﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class DisableCamera : MonoBehaviour
{
    [Header("Script Assigned")]

    [SerializeField]
    private VuforiaBehaviour vuforiaBehaviour;

    [SerializeField]
    private bool isDisabled;

    [Header("Manually Assigned")]

    [SerializeField]
    private GameObject objectsHolder;

    [SerializeField]
    private GameObject lockButton;

    [SerializeField]
    private GameObject infoButton;

    [SerializeField]
    private GameObject otherButtons;

    [SerializeField]
    private Slider animationSlider;

    [SerializeField]
    private Sprite enabledSprite;

    [SerializeField]
    private Sprite disabledSprite;

    [SerializeField]
    private UnityEngine.UI.Image cameraIcon;

    void Start()
    {
        vuforiaBehaviour = FindObjectOfType<VuforiaBehaviour>();
    }

    private void OnEnable()
    {
        ResetCamera();
    }

    public void DisableCameraOnClick()
    {
        if (!isDisabled)
        {
            isDisabled = true;
            cameraIcon.sprite = enabledSprite;

            //componente responsável por 'desligar' a camera
            vuforiaBehaviour.enabled = false;

            if (!lockButton.GetComponent<LockButtonBehaviour>().isLocked || !lockButton.activeSelf)
            {
                ShowObjects();
                ResetPosition();
                lockButton.SetActive(true);
                infoButton.SetActive(true);
                otherButtons.SetActive(true);

                lockButton.GetComponent<LockButtonBehaviour>().ChangeParentOnClick();
                ResetPosition();
            }
        }
        else
        {
            isDisabled = false;
            cameraIcon.sprite = disabledSprite;
            vuforiaBehaviour.enabled = true;
        }
    }

    private void ShowObjects()
    {
        var rendererComponents = objectsHolder.GetComponentsInChildren<Renderer>(true);
        var colliderComponents = objectsHolder.GetComponentsInChildren<Collider>(true);
        var canvasComponents = objectsHolder.GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = true;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;
    }
    public void ResetPosition()
    {
        //coloca o objeto em uma posição definida pelo programador, caso objeto mude de tamanho no futuro, definir aqui
        objectsHolder.transform.localPosition = new Vector3(0f, 0f, 1.5f);
        objectsHolder.transform.localRotation = new Quaternion(0f, 0.15f, 0f, 1f);
        objectsHolder.transform.localScale = new Vector3(1f, 1f, 1f);

        animationSlider.value = 0;
    }

    public void ResetCamera()
    {
        isDisabled = false;
        cameraIcon.sprite = disabledSprite;
    }
}
