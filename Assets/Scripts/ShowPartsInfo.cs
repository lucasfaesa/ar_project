﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowPartsInfo : MonoBehaviour
{
    [Header("Manually Assigned")]

    //usado em GetPartName
    public string partName;

    [SerializeField]
    private List<GameObject> partsNameCanvasObjects;

    [Header("Script Assigned")]

    //usado em infoButtonBehaviour
    public bool isInfoButtonDisabled;

    [SerializeField]
    private bool showingPartsInfo;

    public void ShowPartsNameAndInfo()
    {
        if (!isInfoButtonDisabled)
        {
            if (!showingPartsInfo)
            {
                showingPartsInfo = true;
                for(int i = 0; i < partsNameCanvasObjects.Count; i++)
                {
                    partsNameCanvasObjects[i].SetActive(true);
                }
            }
            else
            {
                showingPartsInfo = false;

                for (int i = 0; i < partsNameCanvasObjects.Count; i++)
                {
                    partsNameCanvasObjects[i].SetActive(false);
                }
            }
        }
    }

    public void HidePartsInfo()
    {
        //desativa informacoes das pecas individuais exibidas
        showingPartsInfo = true;
        ShowPartsNameAndInfo();
    }
}
