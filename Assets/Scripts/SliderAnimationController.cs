﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderAnimationController : MonoBehaviour
{
    [Header("Script Assigned")]

    [SerializeField]
    private TouchController touchController;

    private float timer;

    [Header("Manually Assigned")]

    [SerializeField]
    private Animator carroGrelhaAnimator;

    [SerializeField]
    private Slider slider;

    void Start()
    {
        touchController = FindObjectOfType<TouchController>();
    }

    private void OnEnable()
    {
        //retorna o valor do slider ao inicio toda vez que eh re-exibido o slider em tela
        slider.value = 0;
    }

    private void OnDisable()
    {
        //quando slider for desativado, muda a animação para o estado normal, ou seja, sem a visao explodida
        if(carroGrelhaAnimator != null)
        {
            carroGrelhaAnimator.Play("CarroOtimizadoNormal");
        }
    }

    void Update()
    {
        //controles para que a animação acompanhe o arraste do slider
        carroGrelhaAnimator.Play("CarroOtimizadoExplodido", 0, slider.normalizedValue);
        slider.normalizedValue += timer;

        //gerenciamento para que toda vez que o usuario arrastar pelo slider, o carro não rotacione
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
            {
                touchController.isSlinding = true;
            }
            else { touchController.isSlinding = false; }        
        }
    }
}
