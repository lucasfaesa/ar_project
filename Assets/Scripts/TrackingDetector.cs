﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Vuforia;
using UnityEngine.Video;
using UnityEngine.UI;

public class TrackingDetector : MonoBehaviour, ITrackableEventHandler
{
    [Header("Script Assigned")]

    [SerializeField]
    private TrackableBehaviour mTrackableBehaviour;

    [SerializeField]
    private LockButtonBehaviour lockButtonBehaviour;

    [Header("Manually Assigned")]

    [SerializeField]
    private Button lockButton;

    [SerializeField]
    private Button infoButton;

    void Awake()
    {
        //adaptacao do script original do vuforia para utilizacao dos metodos de deteccao de imagem, colocar script no objeto "Image Target" do vuforia

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    private void Start()
    {
        lockButtonBehaviour = lockButton.GetComponent<LockButtonBehaviour>();
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        //caso objeto esteja sendo trackeado
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            lockButton.gameObject.SetActive(true);
            infoButton.gameObject.SetActive(true);
        }

        //caso objeto tenha perdido o tracking
        else if (previousStatus == TrackableBehaviour.Status.TRACKED && newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            if (!lockButtonBehaviour.isLocked)
            {
                lockButton.gameObject.SetActive(false);
                infoButton.gameObject.SetActive(false);

                lockButtonBehaviour.HidePartsInfoAndResetOutline();
            }
        }
    }
}